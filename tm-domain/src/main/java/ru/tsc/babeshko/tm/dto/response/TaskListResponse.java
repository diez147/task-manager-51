package ru.tsc.babeshko.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractResponse {

    @Nullable
    private List<TaskDTO> tasks;

    public TaskListResponse(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

}