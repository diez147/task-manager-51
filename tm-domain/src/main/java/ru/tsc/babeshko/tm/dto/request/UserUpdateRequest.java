package ru.tsc.babeshko.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserUpdateRequest extends AbstractUserRequest {

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    public UserUpdateRequest(
            @Nullable final String token,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        super(token);
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
    }

}