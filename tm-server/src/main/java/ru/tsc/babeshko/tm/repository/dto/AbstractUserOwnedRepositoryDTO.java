package ru.tsc.babeshko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.babeshko.tm.comparator.CreatedComparator;
import ru.tsc.babeshko.tm.comparator.DateBeginComparator;
import ru.tsc.babeshko.tm.comparator.StatusComparator;
import ru.tsc.babeshko.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;

public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO> extends AbstractRepositoryDTO<M>
        implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else if (comparator == DateBeginComparator.INSTANCE) return "date_begin";
        else return "name";
    }

    public void add(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.persist(model);
    }

    public void update(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) {
        if (userId.isEmpty()) return;
        entityManager.remove(model);
    }

}