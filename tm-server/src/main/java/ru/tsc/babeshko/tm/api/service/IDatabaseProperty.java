package ru.tsc.babeshko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseHbm2ddlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getUseSecondLevelCache();

    @NotNull
    String getUseQueryCache();

    @NotNull
    String getUseMinimalPuts();

    @NotNull
    String getCacheRegionPrefix();

    @NotNull
    String getCacheProviderConfig();

    @NotNull
    String getCacheRegionFactory();

}